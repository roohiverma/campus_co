from django.contrib import admin, messages
from django.db.models import Prefetch

from django.shortcuts import redirect
from django.template.response import TemplateResponse
from django.urls import path
from django.utils.html import format_html

from apps.accounting import resources
from apps.accounting.utils import ImportCSVHelper, VendProductCSVModel, EmployeeCSVModel, ProductTypeCSVModel, \
    VendorCSVModel
from apps.accounting.forms import BaseImportForm, VendProductAdminForm, ProductAdminForm, OrderAdminForm
from . import models
from .models import VendProduct
from .mixins import ExportModelAdminMixin
from django.http import HttpResponseRedirect

import csv,io


class ImportModelAdmin(admin.ModelAdmin):
    import_model = None
    import_form = BaseImportForm
    change_list_template = 'admin/change_list_import.html'

    def get_urls(self):
        urls = super(ImportModelAdmin, self).get_urls()
        add_urls = [
            path(r'import/', self.admin_site.admin_view(self.import_csv))
        ]
        return add_urls + urls
#Importing csv file in djnago admin side
    def import_csv(self, request):
        # if request.method == 'POST':
        #     ImportCSVHelper(self.import_model, request.FILES['csv_file']).save()
        #     return redirect(request.META.get('HTTP_REFERER'))
        context = dict(
            self.admin_site.each_context(request),
            form=self.import_form
        )
        if request.method=='POST':
            csv_file=request.FILES['csv_file']
            if not csv_file.name.endswith('.csv'):
                messages.error(request,'File is not CSV type')
                return HttpResponseRedirect("import")
            # if csv_file.multiple_chunks():
            #     messages.error(request,"Uploaded file is too big (%.2f MB)." % (csv_file.size/(1000*1000),))
            #     return HttpResponseRedirect("import")
            data_set = csv_file.read().decode('UTF-8')
            io_string=io.StringIO(data_set)
            next(io_string)
            for column in csv.reader(io_string,delimiter=',',quotechar='"'):
                if column:
                    print("handle",column[1])
                    created=VendProduct.objects.get_or_create(
                        handle=column[1],
                        sku=column[2],
                        composite_handle=column[3],
                        composite_sku=column[4],
                        composite_quantity=column[5],
                        vend_product_name=column[6],
                        description=column[7],
                        variant_option_one_name=column[8],
                        variant_option_one_value=column[9],
                        variant_option_two_name=column[10],
                        variant_option_two_value=column[11],
                        variant_option_three_name=column[12],
                        variant_option_three_value=column[13],
                        tags=column[15],
                        # supply_price=column[16],
                        # retail_price=column[17],
                        account_code=column[18],
                        account_code_purchase=column[19],
                        )
            messages.success(request, 'CSV File Import successfully')
        return TemplateResponse(request, "admin/import_csv.html")


class EmployeeAdmin(ImportModelAdmin):
    list_display = ('full_name', 'position')
    import_model = EmployeeCSVModel


class VendorAdmin(ImportModelAdmin):
    list_display = ('name',)
    import_model = VendorCSVModel


class PaymentMethodAdmin(admin.ModelAdmin):
    list_display = ('name',)


class ProductTypeAdmin(ImportModelAdmin):
    list_display = ('name', 'brand')
    import_model = ProductTypeCSVModel


class AttachmentAdmin(admin.StackedInline):
    model = models.Attachment


class OrderAdmin(admin.ModelAdmin, ExportModelAdminMixin):
    resource_class = resources.OrderResource
    list_display = (
        'id', 'vendor', 'purchase_date', 'complete', 'imported', 'products', 'receipt_total', 'order_total')
    inlines = [AttachmentAdmin]
    list_filter = (
        ('deleted', admin.BooleanFieldListFilter),
    )
    readonly_fields = ('order_subtotal', 'taxable_amount')
    actions = ('restore_deleted', 'mark_as_imported')
    change_list_template = 'admin/change_list_override.html'
    form = OrderAdminForm
    list_per_page = 50

    def get_queryset(self, request):
        return super().get_queryset(request).prefetch_related(
            Prefetch('product_set', queryset=models.Product.objects.filter(deleted=False))
        )

    def mark_as_imported(self, request, queryset):
        return queryset.update(imported=True)

    def changelist_view(self, request, extra_context=None):
        if not request.GET.get('deleted__exact', None):
            if '?' not in request.path:
                return redirect(f'{request.path}?deleted__exact=0')
            else:
                return redirect(f'{request.path}&deleted__exact=0')
        return super().changelist_view(request, extra_context)

    def delete_queryset(self, request, queryset):
        if len(queryset) > 1:
            models.Product.objects.filter(order_id__in=[i.id for i in queryset]).update(deleted=True)
        elif len(queryset) == 1:
            models.Product.objects.filter(order_id=queryset.first().id).update(deleted=True)
        queryset.update(deleted=True)

    def restore_deleted(self, request, queryset):
        if len(queryset) > 1:
            models.Product.objects.filter(order_id__in=[i.id for i in queryset]).update(deleted=False)
        elif len(queryset) == 1:
            models.Product.objects.filter(order_id=queryset.first().id).update(deleted=False)
        queryset.update(deleted=False)

    restore_deleted.short_description = 'Restore'

    def products(self, obj):
        return format_html('<a target="_blank" href="/admin/accounting/product/?order__id__exact={id}">{count}</a>',
                           id=obj.id,
                           count=len(obj.product_set.all()))

    def save_form(self, request, form, change):
        """
                Given a ModelForm return an unsaved instance. ``change`` is True if
                the object is being changed, and False if it's being added.
                """
        instance = form.save(commit=False)
        if not instance:
            messages.warning(request, 'Editing of completed order is not allowed.')
        return instance


class VendProductAdmin(ImportModelAdmin, ExportModelAdminMixin):
    form = VendProductAdminForm
    delete_confirmation_template = 'admin/delete_confirmation_override.html'
    resource_class = resources.VendProductResource
    import_model = VendProductCSVModel
    list_display = ('get_sku', 'vend_product_name', 'handle', 'retail_price', 'new')
    list_filter = ('new',)
    actions = ('mark_new',)
    search_fields = ('sku', 'vend_product_name')

    def get_sku(self, obj):
        return obj.sku or f'Empty sku, ID: {obj.id}'

    get_sku.short_description = 'SKU'

    def mark_new(self, request, queryset):
        return queryset.update(new=False)

    mark_new.short_description = 'Mark new products'

    def delete_queryset(self, request, queryset):
        queryset.delete()

    def log_deletion(self, request, object, object_repr):
        pass


class BaseProductAdmin(admin.ModelAdmin, ExportModelAdminMixin):
    def vend_product_name(self, obj: models.Product):
        return obj.name or f'Empty name, ID: {obj.id}'

    def product_description(self, obj: models.Product):
        return obj.description


class ProductAdmin(BaseProductAdmin):
    resource_class = resources.ProductResource
    list_display = ('vend_product_name', 'product_description', 'purchased_by',
                    'vendor', 'sku', 'purchase_quantity', 'expiration_date', 'retail_quantity', 'vend_cost_each')
    list_filter = (
        ('order', admin.RelatedOnlyFieldListFilter),
        ('deleted', admin.BooleanFieldListFilter)
    )
    # readonly_fields = ('retail_price',)
    actions = ('restore_deleted',)
    change_list_template = 'admin/change_list_override.html'
    list_per_page = 50
    form = ProductAdminForm

    def get_queryset(self, request):
        return super().get_queryset(request).select_related(
            'order__purchased_by', 'order__vendor', 'type').prefetch_related(
            Prefetch('order__product_set', queryset=models.Product.objects.filter(deleted=False))
        )

    def changelist_view(self, request, extra_context=None):
        if not request.GET.get('deleted__exact', None):
            params = '?deleted__exact=0'
            for k, v in request.GET.items():
                params = '{}&{}={}'.format(params, k, v)
            return redirect(f'{request.path}{params}')
        return super().changelist_view(request, extra_context)

    def delete_queryset(self, request, queryset):
        queryset.update(deleted=True)

    def restore_deleted(self, request, queryset):
        queryset.update(deleted=False)

    restore_deleted.short_description = 'Restore'


class ExistingProductAdmin(BaseProductAdmin):
    resource_class = resources.ExistingProductResource
    list_display = ('vend_product_name', 'purchase_date',
                    'variant_value', 'vendor', 'sku', 'purchase_quantity', 'expiration_date',)

    def get_queryset(self, request):
        return super().get_queryset(request).filter(expiration_date__isnull=False).select_related('purchased_by',
                                                                                                  'vendor')

    def variant_value(self, obj):
        return obj.weight


class BrandAdmin(admin.ModelAdmin):
    list_display = ('name', 'product_types')

    def product_types(self, obj):
        return obj.producttype_set.count()


admin.site.register(models.Employee, EmployeeAdmin)
admin.site.register(models.Vendor, VendorAdmin)
admin.site.register(models.PaymentMethod, PaymentMethodAdmin)
admin.site.register(models.Order, OrderAdmin)
admin.site.register(models.ProductType, ProductTypeAdmin)
admin.site.register(models.VendProduct, VendProductAdmin)
admin.site.register(models.Product, ProductAdmin)
admin.site.register(models.ExistingProduct, ExistingProductAdmin)
admin.site.register(models.Brand, BrandAdmin)
admin.site.register(models.Variant)
