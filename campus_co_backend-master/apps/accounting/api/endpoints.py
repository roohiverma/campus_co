from django.db import transaction
from django.db.models import Prefetch
from rest_framework import generics, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from apps.accounting import models
from . import serializers


class ListEmployeesAPIView(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = models.Employee.objects.all()
    serializer_class = serializers.EmployeeSerializer


class ListVendorsAPIView(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = models.Vendor.objects.all()
    serializer_class = serializers.VendorSerializer


class ListPaymentMethodsAPIView(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = models.PaymentMethod.objects.all()
    serializer_class = serializers.PaymentMethodSerializer


class ListProductTypesAPIView(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = models.ProductType.objects.all()
    serializer_class = serializers.ProductTypeSerializer


class ListCreateOrderAPIView(generics.ListCreateAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = models.Order.objects.filter(deleted=False).prefetch_related(
            Prefetch('product_set', queryset=models.Product.objects.filter(deleted=False))
        )

    def get_serializer_class(self):
        if getattr(self, 'action', None):
            if self.action == 'create':
                return serializers.CreateUpdateOrderSerializer
            else:
                return serializers.ListRetrieveOrderSerializer
        return serializers.ListRetrieveOrderSerializer


class RetrieveOrderAPIView(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = models.Order.objects.all()
    lookup_field = 'pk'

    def get_serializer_class(self):
        if getattr(self, 'method', None):
            if self.request.method == 'PUT':
                return serializers.CreateUpdateOrderSerializer
            else:
                return serializers.ListRetrieveOrderSerializer
        return serializers.ListRetrieveOrderSerializer

    def perform_destroy(self, instance):
        instance.deleted = True
        instance.save()
        models.Product.objects.filter(order_id=instance.id).update(deleted=True)


class AddOrderAttachmentAPIView(generics.CreateAPIView):
    queryset = models.Attachment.objects.all()
    serializer_class = serializers.CreateAttachmentSerializer
    lookup_field = 'order_id'

    def create(self, request, *args, **kwargs):
        request.data['order'] = kwargs.get('order_id')
        return super().create(request, *args, **kwargs)


class DestroyOrderAttachmentAPIView(generics.DestroyAPIView):
    queryset = models.Attachment.objects.all()
    serializer_class = serializers.CreateAttachmentSerializer
    lookup_field = 'pk'


class SearchVendProductAPIView(generics.GenericAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = models.VendProduct.objects.all()
    serializer_class = serializers.SearchVendProductSerializer

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        sku = serializer.validated_data['sku']
        products = models.VendProduct.objects.filter(sku=sku)
        response_serializer = serializers.VendProductSerializer(products, many=True)
        return Response(response_serializer.data)


class ListCreateProductAPIView(generics.ListCreateAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = models.Product.objects.filter(deleted=False)
    serializer_class = serializers.ProductSerializer

    def get_queryset(self):
        order_id = self.kwargs.get('order_id')
        return super().get_queryset().filter(order_id=order_id)

    @transaction.atomic
    def create(self, request, *args, **kwargs):
        data = request.data
        data['order'] = kwargs.get('order_id')
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def perform_create(self, serializer):
        return serializer.save()


class RetrieveUpdateProductAPIView(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = models.Product.objects.existing()
    serializer_class = serializers.ProductSerializer
    lookup_field = 'pk'

    def perform_destroy(self, instance):
        instance.deleted = True
        instance.save()


class ListBrandsAPIView(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = models.Brand.objects.all()
    serializer_class = serializers.BrandSerializer


class ListVendVariantsAPIView(generics.ListAPIView):
    queryset = models.VendProduct.objects.all()
    permission_classes = (IsAuthenticated,)
    serializer_class = serializers.VendProductSerializer

    def get_queryset(self):
        handle = self.request.query_params.get('handle')
        if not handle:
            return []
        return super().get_queryset().filter(handle=handle)


class ListVariantsAPIView(generics.ListAPIView):
    queryset = models.Variant.objects.all()
    permission_classes = (IsAuthenticated,)
    serializer_class = serializers.VariantSerializers
