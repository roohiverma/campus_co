import pytz
from drf_extra_fields.fields import Base64ImageField
from rest_framework import serializers
from apps.accounting import models


class EmployeeSerializer(serializers.ModelSerializer):
    name = serializers.CharField(source='full_name')

    class Meta:
        model = models.Employee
        fields = ('id', 'name', 'position')


class VendorSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Vendor
        fields = ('id', 'name')


class PaymentMethodSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.PaymentMethod
        fields = ('id', 'name')


class ProductTypeSerializer(serializers.ModelSerializer):
    brand = serializers.SerializerMethodField()

    class Meta:
        model = models.ProductType
        fields = ('id', 'name', 'brand')

    def get_brand(self, obj):
        if not obj.brand:
            return ''
        return obj.brand.name


class RetrieveAttachmentSerializer(serializers.ModelSerializer):
    attachment = serializers.ImageField(required=False, use_url=True)
    
    class Meta:
        model = models.Attachment
        fields = ('id', 'attachment',)


class CreateAttachmentSerializer(serializers.ModelSerializer):
    attachment = Base64ImageField(required=False)
    
    class Meta:
        model = models.Attachment
        fields = ('attachment', 'order')


class CreateUpdateOrderSerializer(serializers.ModelSerializer):
    extra_charges = serializers.DecimalField(max_digits=10, decimal_places=3, required=False, default=0)
    discounts = serializers.DecimalField(max_digits=10, decimal_places=3, required=False, default=0)
    tax_amount = serializers.DecimalField(max_digits=10, decimal_places=3, required=False, default=0)

    class Meta:
        model = models.Order
        fields = ('purchased_by', 'vendor', 'purchase_date', 'entered_by', 'payment_method',
                  'extra_charges', 'discounts', 'tax_amount', 'receipt_total', 'comments',)


class ListRetrieveOrderSerializer(serializers.ModelSerializer):
    products_count = serializers.SerializerMethodField()
    order_total = serializers.DecimalField(max_digits=10, decimal_places=2, read_only=True)
    attachments = RetrieveAttachmentSerializer(many=True, read_only=True)
    created = serializers.DateTimeField(format='%Y-%m-%d %H:%M:%S', default_timezone=pytz.timezone('America/Chicago'), read_only=True)
    modified = serializers.DateTimeField(format='%Y-%m-%d %H:%M:%S', read_only=True)

    class Meta:
        model = models.Order
        fields = '__all__'

    def get_products_count(self, obj):
        return len(obj.product_set.all())


class VendProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.VendProduct
        fields = '__all__'


class VariantSerializers(serializers.ModelSerializer):
    class Meta:
        model = models.Variant
        fields = '__all__'


class SearchVendProductSerializer(serializers.Serializer):
    sku = serializers.CharField()

    class Meta:
        fields = ('sku', )


class ProductSerializer(serializers.ModelSerializer):
    vend_cost_each = serializers.SerializerMethodField()
    retail_quantity = serializers.SerializerMethodField()
    retail_price = serializers.SerializerMethodField()
    brand = serializers.PrimaryKeyRelatedField(queryset=models.Brand.objects.all(), required=False)

    class Meta:
        model = models.Product
        fields = '__all__'

    def get_vend_cost_each(self, obj):
        return obj.vend_cost_each

    def get_retail_quantity(self, obj):
        return obj.retail_quantity

    def get_retail_price(self, obj):
        return obj.retail_price


class ExistingProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.ExistingProduct
        fields = '__all__'


class BrandProductTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.ProductType
        fields = ('id', 'name',)


class BrandSerializer(serializers.ModelSerializer):
    product_types = BrandProductTypeSerializer(source='producttype_set', many=True)

    class Meta:
        model = models.Brand
        fields = ('id', 'name', 'product_types')
