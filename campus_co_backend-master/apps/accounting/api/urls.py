from django.urls import path
from . import endpoints

urlpatterns = [
    path('employees/', endpoints.ListEmployeesAPIView.as_view(), name='employees'),
    path('vendors/', endpoints.ListVendorsAPIView.as_view(), name='vendors'),
    path('payment-methods/', endpoints.ListPaymentMethodsAPIView.as_view(), name='payment-methods'),
    path('product-types/', endpoints.ListProductTypesAPIView.as_view(), name='product-types'),
    path('brands/', endpoints.ListBrandsAPIView.as_view(), name='brands'),
    path('orders/', endpoints.ListCreateOrderAPIView.as_view(), name='orders'),
    path('vend-products/', endpoints.SearchVendProductAPIView.as_view(), name='vend-products'),
    path('vend-products/variants/', endpoints.ListVendVariantsAPIView.as_view(), name='vend-products-variants'),
    path('orders/<int:pk>/', endpoints.RetrieveOrderAPIView.as_view(), name='retrieve-order'),
    path('orders/<int:order_id>/products/', endpoints.ListCreateProductAPIView.as_view(), name='order-products'),
    path('orders/<int:order_id>/attachments/', endpoints.AddOrderAttachmentAPIView.as_view(),
         name='add-order-attachment'),
    path('orders/<int:order_id>/attachments/<int:pk>/', endpoints.DestroyOrderAttachmentAPIView.as_view(),
         name='destroy-order-attachment'),
    path('products/<int:pk>/', endpoints.RetrieveUpdateProductAPIView.as_view(), name='products-product'),
    path('variants/', endpoints.ListVariantsAPIView.as_view(), name='variants'),
]
