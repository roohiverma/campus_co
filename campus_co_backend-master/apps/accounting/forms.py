from django import forms

from apps.accounting import models


class BaseImportForm(forms.Form):
    csv_file = forms.FileField()


class VendProductForm(forms.ModelForm):
    class Meta:
        model = models.VendProduct
        fields = ('sku', 'vend_product_name', 'retail_price')


class EmployeeForm(forms.ModelForm):
    class Meta:
        model = models.Employee
        fields = ('first_name', 'last_name')


class ProductTypeForm(forms.ModelForm):
    class Meta:
        model = models.ProductType
        fields = ('name',)


# Admin forms #

class VendProductAdminForm(forms.ModelForm):
    brand = forms.ModelChoiceField(label='Vend Brand', queryset=models.Brand.objects.all())

    class Meta:
        model = models.VendProduct
        fields = '__all__'


class ProductAdminForm(forms.ModelForm):

    class Meta:
        model = models.Product
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.instance:
            self.fields['_retail_price'].initial = self.instance.retail_price
            self.initial['_retail_price'] = self.instance.retail_price


class OrderAdminForm(forms.ModelForm):
    class Meta:
        model = models.Order
        fields = '__all__'

    def save(self, commit=True):
        if self.instance and self.cleaned_data:
            if self.instance.complete and 'complete' not in self.changed_data:
                return
        return super().save(commit)


