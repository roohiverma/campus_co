# Generated by Django 2.1.3 on 2018-11-28 21:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounting', '0002_auto_20181128_2041'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='comments',
            field=models.TextField(blank=True, verbose_name='comments'),
        ),
    ]
