# Generated by Django 2.1.3 on 2018-11-30 09:19

from django.db import migrations, models
import django.db.models.deletion
import django_extensions.db.fields


class Migration(migrations.Migration):

    dependencies = [
        ('accounting', '0006_product_order'),
    ]

    operations = [
        migrations.CreateModel(
            name='ExitingProduct',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', django_extensions.db.fields.CreationDateTimeField(auto_now_add=True, verbose_name='created')),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(auto_now=True, verbose_name='modified')),
                ('purchase_quantity', models.IntegerField(default=1, verbose_name='purchase quantity')),
                ('coast_each', models.DecimalField(decimal_places=2, max_digits=10, verbose_name='coast each')),
                ('we_payed_tax', models.BooleanField(default=False, verbose_name='we payed tax')),
                ('taxable_product', models.BooleanField(default=False, verbose_name='taxable product')),
                ('new_product', models.BooleanField(default=False, verbose_name='new product')),
                ('brand_name', models.CharField(blank=True, max_length=255, verbose_name='brand name')),
                ('flavor', models.CharField(blank=True, max_length=255, verbose_name='brand name')),
                ('description', models.CharField(blank=True, max_length=255, verbose_name='brand name')),
                ('bought_by_pound', models.BooleanField(default=False, verbose_name='bought by the pound')),
                ('expiration_date', models.DateField(verbose_name='expiration date')),
                ('sku', models.CharField(max_length=50, verbose_name='sku')),
                ('create_vend_sku', models.BooleanField(default=False, verbose_name='create new vend sku')),
                ('needs_repacking', models.NullBooleanField(default=None, verbose_name='needs repacking')),
                ('units_per_bulk', models.IntegerField(blank=True, null=True, verbose_name='retail units per bulk unit')),
                ('retail_price', models.DecimalField(blank=True, decimal_places=2, max_digits=10, null=True, verbose_name='coast each')),
                ('comments', models.TextField(blank=True, verbose_name='comments')),
                ('purchased_by', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='purchased_exiting_products', to='accounting.Employee', verbose_name='purchased by')),
                ('type', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='accounting.ProductType')),
                ('vendor', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='exiting_products', to='accounting.Vendor', verbose_name='vendor')),
            ],
            options={
                'verbose_name': 'Exiting product',
                'verbose_name_plural': 'Exiting products',
            },
        ),
    ]
