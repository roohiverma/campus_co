# Generated by Django 2.1.3 on 2018-12-06 21:28

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounting', '0017_auto_20181206_2030'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='vendproduct',
            name='description',
        ),
        migrations.AddField(
            model_name='existingproduct',
            name='weight',
            field=models.CharField(blank=True, max_length=255, verbose_name='weight'),
        ),
        migrations.AddField(
            model_name='product',
            name='volume',
            field=models.CharField(blank=True, max_length=255, verbose_name='volume/ weight/ count'),
        ),
        migrations.AddField(
            model_name='vendproduct',
            name='vend_product_name',
            field=models.CharField(blank=True, max_length=255, verbose_name='vend product name'),
        ),
    ]
