# Generated by Django 2.1.5 on 2019-10-09 09:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounting', '0036_auto_20190307_2117'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='_retail_price',
            field=models.DecimalField(blank=True, decimal_places=3, max_digits=10, null=True, verbose_name='Retail price'),
        ),
        migrations.AlterField(
            model_name='vendproduct',
            name='retail_price',
            field=models.CharField(blank=True, max_length=10, null=True, verbose_name='retail price'),
        ),
    ]
