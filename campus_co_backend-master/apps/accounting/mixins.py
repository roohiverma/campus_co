from import_export.admin import ExportMixin
from import_export.formats.base_formats import CSV

from django.http import HttpResponse


class ExportModelAdminMixin(ExportMixin):
    actions = ['export']

    def export(self, request, queryset):
        file_format = CSV()
        data = self.resource_class().export(queryset)
        export_data = file_format.export_data(data)

        content_type = file_format.get_content_type()
        response = HttpResponse(export_data, content_type=content_type)
        response['Content-Disposition'] = 'attachment; filename=%s' % (
            self.get_export_filename(file_format),
        )

        return response

    export.short_description = "Export to CSV"
