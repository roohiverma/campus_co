import os
import uuid
from decimal import Decimal, ROUND_HALF_UP

from django.db import models
from django.db.models.sql import DeleteQuery
from django_extensions.db.models import TimeStampedModel
from model_utils import FieldTracker

from django.core.cache import cache

from apps.accounting.services import create_products_on_order_complete
from apps.accounting.notifications import notify_on_order_imported, notify_on_order_complete


class Employee(models.Model):
    POSITION_EMPLOYEE = 1

    POSITIONS = (
        (POSITION_EMPLOYEE, 'employee'),
    )

    first_name = models.CharField('first name', max_length=100)
    last_name = models.CharField('last name', max_length=100)
    position = models.SmallIntegerField('position', choices=POSITIONS, default=POSITION_EMPLOYEE)

    class Meta:
        verbose_name = 'Employee'
        verbose_name_plural = 'Employees'

    def __str__(self):
        return self.full_name

    @property
    def full_name(self):
        return f'{self.first_name} {self.last_name}'


class Vendor(models.Model):
    name = models.CharField('name', max_length=100)

    class Meta:
        verbose_name = 'Vendor'
        verbose_name_plural = 'Vendors'

    def __str__(self):
        return self.name


class PaymentMethod(models.Model):
    name = models.CharField('name', max_length=100)

    class Meta:
        verbose_name = 'Payment method'
        verbose_name_plural = 'Payment methods'

    def __str__(self):
        return self.name


class ProductManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().select_related('order')

    def existing(self):
        queryset = super().get_queryset().filter(deleted=False)
        return queryset


class Order(TimeStampedModel):
    purchased_by = models.ForeignKey('accounting.Employee',
                                     verbose_name='purchased by',
                                     related_name='purchased_orders',
                                     on_delete=models.CASCADE)
    vendor = models.ForeignKey('accounting.Vendor',
                               verbose_name='vendor',
                               related_name='orders',
                               on_delete=models.CASCADE)
    purchase_date = models.DateField('purchase date')
    entered_by = models.ForeignKey('accounting.Employee',
                                   verbose_name='entered by',
                                   related_name='entered_orders',
                                   on_delete=models.CASCADE)
    payment_method = models.ForeignKey('accounting.PaymentMethod',
                                       verbose_name='payment method',
                                       related_name='orders',
                                       on_delete=models.CASCADE)
    extra_charges = models.DecimalField('extra charges', max_digits=10, decimal_places=3, default=0)
    discounts = models.DecimalField('discounts', max_digits=10, decimal_places=3, default=0)
    tax_amount = models.DecimalField('tax amount', max_digits=10, decimal_places=3, default=0)
    receipt_total = models.DecimalField('receipt total', max_digits=10, decimal_places=3)
    comments = models.TextField('comments', blank=True)

    deleted = models.BooleanField(default=False)

    imported = models.BooleanField(default=False)
    complete = models.BooleanField(default=False)

    tracker = FieldTracker(fields=['imported', 'complete'])

    class Meta:
        verbose_name = 'Order'
        verbose_name_plural = 'Orders'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._products = None

    def __str__(self):
        return f'{self.id}'

    def save(self, **kwargs):
        if self.tracker.has_changed('imported') and self.imported:
            notify_on_order_imported(self)
        if self.tracker.has_changed('complete') and self.complete:
            notify_on_order_complete(self)
            create_products_on_order_complete(self)
        super().save(**kwargs)

    def get_products(self):
        if self._products:
            return self._products
        key = f'order_{self.pk}_products'
        products = cache.get(key)
        if not products or len(products) != len(self.product_set.all()):
            products = [{
                'cost_each': p.cost_each,
                'purchase_quantity': p.purchase_quantity,
                'we_payed_tax': p.we_payed_tax} for p in self.product_set.all()]
            cache.set(key, products, 60 * 60 * 24 * 5)
        self._products = products
        return products

    @property
    def order_subtotal(self):
        total = sum((p['cost_each'] * p['purchase_quantity'] for p in self.get_products()), Decimal('0'))
        return Decimal(total.quantize(Decimal('.01'), rounding=ROUND_HALF_UP))

    @property
    def order_total(self):
        total = sum((p['cost_each'] * p['purchase_quantity'] for p in self.get_products()), Decimal('0'))
        total = total + self.tax_amount + self.extra_charges - self.discounts
        return Decimal(total.quantize(Decimal('.01'), rounding=ROUND_HALF_UP))

    @property
    def taxable_amount(self):
        val = sum((p['cost_each'] * p['purchase_quantity'] for p in self.get_products() if p['we_payed_tax']), Decimal('0'))
        return Decimal(Decimal(str(val)).quantize(Decimal('.01'), rounding=ROUND_HALF_UP))


def attachment_upload_path(instance, filename):
    name, ext = os.path.splitext(filename)
    name = str(uuid.uuid4())
    return f'{os.path.sep}'.join(('orders', str(instance.order.id), 'attachments', f'{name}{ext}'))


class Attachment(models.Model):
    attachment = models.ImageField(upload_to=attachment_upload_path, blank=True, null=True)
    order = models.ForeignKey('accounting.Order', related_name='attachments', on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Attachment'
        verbose_name_plural = 'Attachments'

    def __str__(self):
        return f'{self.id}'


class ProductType(models.Model):
    name = models.CharField('name', max_length=100)
    brand = models.ForeignKey('accounting.Brand',
                              null=True,
                              on_delete=models.SET_NULL)

    class Meta:
        verbose_name = 'Product type'
        verbose_name_plural = 'Product types'

    def __str__(self):
        return f'{self.name}'


class Variant(models.Model):
    name = models.CharField('name', max_length=255, blank=True)

    class Meta:
        verbose_name = 'Variant'
        verbose_name_plural = 'Variants'

    def __str__(self):
        return self.name


class BaseProduct(TimeStampedModel):
    purchase_quantity = models.IntegerField('purchase quantity', default=1)
    cost_each = models.DecimalField('cost each', max_digits=10, decimal_places=3, blank=True, null=True)
    we_payed_tax = models.BooleanField('we payed tax', default=False)
    taxable_product = models.BooleanField('taxable product', default=False)
    brand = models.ForeignKey('accounting.Brand',
                              verbose_name='Vend Brand',
                              max_length=255,
                              blank=True,
                              null=True,
                              on_delete=models.SET_NULL)
    flavor = models.CharField('flavor', max_length=255, blank=True)
    name = models.CharField('name', max_length=255, blank=True)
    description = models.CharField('description', max_length=255, blank=True)
    bought_by_pound = models.BooleanField('bought by the pound', default=False)
    expiration_date = models.DateField('expiration date', blank=True, null=True)
    sku = models.CharField('sku', max_length=50, blank=True)
    type = models.ForeignKey('accounting.ProductType',
                             null=True,
                             blank=True,
                             on_delete=models.SET_NULL)

    # Bulk orders fields
    needs_repacking = models.BooleanField('needs repacking', default=False)
    units_per_bulk = models.IntegerField('retail units per bulk unit', blank=True, null=True)
    comments = models.TextField('comments', blank=True)

    class Meta:
        abstract = True

    def save(self, **kwargs):
        if not self.sku:
            self.sku = f'CTRWF-{Product.objects.count()}'
        super().save(**kwargs)


class Product(BaseProduct):
    order = models.ForeignKey('accounting.Order', verbose_name='order', null=True, on_delete=models.CASCADE)
    volume = models.CharField('volume/ weight/ count', max_length=255, blank=True)
    _retail_price = models.DecimalField('Retail price', max_digits=10, decimal_places=3, blank=True, null=True)
    custom_retail_price = models.BooleanField(default=False)

    deleted = models.BooleanField(default=False)

    objects = ProductManager()

    class Meta:
        verbose_name = 'Product'
        verbose_name_plural = 'Products'

    def save(self, **kwargs):
        key = f'order_{self.order_id}_products'
        cache.delete(key)
        super().save(**kwargs)

    @property
    def retail_price(self):
        if self.custom_retail_price:
            return self._retail_price
        return Decimal((self.vend_cost_each / Decimal('0.65')).quantize(Decimal('.1'),
                                                                        rounding=ROUND_HALF_UP))

    @retail_price.setter
    def retail_price(self, val):
        self._retail_price = val

    @property
    def purchased_by(self):
        return self.order.purchased_by

    @property
    def vendor(self):
        return self.order.vendor

    @property
    def retail_quantity(self):
        return self.purchase_quantity * (self.units_per_bulk or 1)

    @property
    def extra_charges(self):
        val = Decimal(str(self.cost_each or 0)) * self.purchase_quantity / (self.order.order_subtotal or 1) * self.order.extra_charges
        return Decimal(Decimal(str(val)).quantize(Decimal('.001'), rounding=ROUND_HALF_UP))

    @property
    def discounts(self):
        val = Decimal(str(self.cost_each or 0)) * self.purchase_quantity / (self.order.order_subtotal or 1) * self.order.discounts
        return Decimal(Decimal(str(val)).quantize(Decimal('.001'), rounding=ROUND_HALF_UP))

    @property
    def tax_amount(self):
        if not self.we_payed_tax:
            return Decimal('0')
        val = (self.cost_each or 0) * self.purchase_quantity / (self.order.taxable_amount or 1) * self.order.tax_amount
        return Decimal(Decimal(str(val)).quantize(Decimal('.001'), rounding=ROUND_HALF_UP))

    @property
    def vend_cost_each(self):
        val = (((self.cost_each or 0) * self.purchase_quantity) + self.tax_amount + self.extra_charges - self.discounts)
        val /= self.retail_quantity
        return Decimal(Decimal(str(val)).quantize(Decimal('.001'), rounding=ROUND_HALF_UP))

    def __str__(self):
        return self.name or f'Empty name, ID: {self.id}'


class ExistingProduct(BaseProduct):
    purchased_by = models.ForeignKey('accounting.Employee',
                                     verbose_name='purchased by',
                                     related_name='purchased_existing_products',
                                     null=True,
                                     on_delete=models.SET_NULL)
    vendor = models.ForeignKey('accounting.Vendor',
                               verbose_name='vendor',
                               related_name='existing_products',
                               null=True,
                               on_delete=models.SET_NULL)
    weight = models.CharField('weight', max_length=255, blank=True)
    retail_price = models.DecimalField('retail price', max_digits=10, decimal_places=2, blank=True, null=True)
    purchase_date = models.DateField('purchase date', blank=True, null=True)

    class Meta:
        verbose_name = 'Expiry product'
        verbose_name_plural = 'Expiry products'

    def __str__(self):
        if self.sku.replace(' ', ''):
            return self.sku
        return f'Empty sku, ID: {self.id}'

    @classmethod
    def from_product(cls, product: Product, purchased_by, vendor):
        return cls(
            purchased_by=purchased_by,
            vendor=vendor,
            weight=product.volume,
            purchase_quantity=product.purchase_quantity,
            cost_each=product.cost_each,
            we_payed_tax=product.we_payed_tax,
            taxable_product=product.taxable_product,
            brand_id=product.brand_id,
            flavor=product.flavor,
            name=product.name,
            description=product.description,
            bought_by_pound=product.bought_by_pound,
            expiration_date=product.expiration_date,
            sku=product.sku,
            type=product.type,
            needs_repacking=product.needs_repacking,
            units_per_bulk=product.units_per_bulk,
            retail_price=product.retail_price,
            comments=product.comments,
            purchase_date=product.order.purchase_date
        )


class BulkDeleteQuery(DeleteQuery):
    def delete_batch(self, using, field=None):
        if not field:
            field = self.model._meta.pk

        self.do_query(self.model._meta.db_table, self.where, using=using)


class VendProductManager(models.Manager):
    def bulk_delete(self, queryset):
        delete_query = queryset.query.clone(BulkDeleteQuery)
        delete_query.delete_batch(using=queryset.db)


class VendProduct(TimeStampedModel):
    sku = models.CharField('sku', max_length=50, blank=True)
    vend_product_name = models.CharField('vend product name', max_length=255, blank=True)
    handle = models.CharField('handle', max_length=255, blank=True)
    composite_handle = models.CharField('composite handle', max_length=255, blank=True)
    composite_sku = models.CharField('composite sku', max_length=255, blank=True)
    composite_quantity = models.CharField('composite quantity', max_length=10, blank=True, default='')
    description = models.TextField('description', blank=True)
    variant_option_one_name = models.CharField('variant option one name', max_length=255, default="Size", blank=True)
    variant_option_one_value = models.CharField('variant option one value', max_length=255, blank=True)
    variant_option_two_name = models.CharField('variant option two name', max_length=255, blank=True)
    variant_option_two_value = models.CharField('variant option two value', max_length=255, blank=True)
    variant_option_three_name = models.CharField('variant option three name', max_length=255, blank=True)
    variant_option_three_value = models.CharField('variant option three value', max_length=255, blank=True)
    supply_price = models.DecimalField('supply price', max_digits=10, decimal_places=3, blank=True, null=True)
    tags = models.CharField('tags', max_length=255, blank=True)
    outlet_tax_redwood_falls = models.CharField('outlet tax redwood falls', max_length=255, blank=True)
    account_code = models.CharField('account code', max_length=255, default='4510')
    account_code_purchase = models.CharField('account code purchase', max_length=255, default='1310')
    vendor = models.ForeignKey('accounting.Vendor',
                               verbose_name='vendor',
                               related_name='vend_products',
                               null=True,
                               on_delete=models.SET_NULL)
    active = models.CharField(max_length=3, default='1')
    track_inventory = models.CharField(max_length=3, default='1')
    type = models.ForeignKey('accounting.ProductType',
                             null=True,
                             on_delete=models.SET_NULL)
    brand = models.ForeignKey('accounting.Brand',
                              verbose_name='Vend Brand',
                              max_length=255,
                              blank=True,
                              null=True,
                              on_delete=models.SET_NULL)
    retail_price = models.DecimalField('retail price', max_digits=10, decimal_places=2, blank=True, null=True)
    new = models.BooleanField(default=True)

    objects = VendProductManager()

    class Meta:
        verbose_name = 'Vend product'
        verbose_name_plural = 'Vend products'

    def __str__(self):
        if self.sku.replace(' ', ''):
            return self.sku
        return f'Empty sku, ID: {self.id}'

    @classmethod
    def from_product(cls, product: Product, vendor):
        return cls(
            sku=product.sku,
            handle=''.join([i for i in product.name or '' if i.isalpha() or i.isdigit()]),
            composite_handle='',
            composite_sku='',
            composite_quantity='',
            vend_product_name=product.name,
            description='',
            type=product.type,
            variant_option_one_value=product.volume,
            variant_option_two_name='',
            variant_option_two_value='',
            variant_option_three_name='',
            variant_option_three_value='',
            tags='',
            supply_price=product.vend_cost_each,
            retail_price=product.retail_price,
            outlet_tax_redwood_falls='Tax Exempt' if not product.taxable_product else 'Default Tax',
            brand_id=product.brand_id,
            vendor=vendor,
        )


class Brand(models.Model):
    name = models.CharField('name', max_length=100)

    class Meta:
        verbose_name = 'Brand'
        verbose_name_plural = 'Brands'

    def __str__(self):
        return f'{self.name}'
