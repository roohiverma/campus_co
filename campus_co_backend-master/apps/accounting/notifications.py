from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.mail import send_mail, EmailMultiAlternatives
from django.template.loader import render_to_string
from django.utils.html import strip_tags

User = get_user_model()


def notify_on_order_imported(order):
    emails = [u.email for u in User.objects.filter(notify_on_order_imported=True)]
    order_link = f"{settings.SITE_SHAME}://{settings.SITE_HOST}/admin/accounting/order/{order.id}/change/"
    send_mail(f"Order {order.id} imported!",
              f"""Order with ID {order.id} Vendor Name {order.vendor.name} Purchased by {order.purchased_by.full_name} for the amount of {order.receipt_total} was imported! 
              See the order here: {order_link}""",
              f"{settings.ADMIN_SITE_HEADER} <ben@ntstiresupply.com>", emails)


def notify_on_order_complete(order):
    products = order.product_set.filter(deleted=False, needs_repacking=True)
    if not products:
        return
    emails = [u.email for u in User.objects.filter(notify_on_order_complete=True)]
    subject, from_email = f"Order {order.id} complete!", f"{settings.ADMIN_SITE_HEADER} <ben@ntstiresupply.com>"
    order_link = f"{settings.SITE_SHAME}://{settings.SITE_HOST}/admin/accounting/order/{order.id}/change/"
    html_content = render_to_string('email/order_complete_email.html', {
        'order': order,
        'products': products,
        'link': order_link})
    text_content = strip_tags(html_content)
    msg = EmailMultiAlternatives(subject, text_content, from_email, emails)
    msg.attach_alternative(html_content, "text/html")
    msg.send()
