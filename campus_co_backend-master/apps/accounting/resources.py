from import_export import resources
from import_export.fields import Field

from apps.accounting import models


class VendProductResource(resources.ModelResource):
    id = Field(column_name='id')
    sku = Field(attribute='sku', column_name='sku')
    name = Field(attribute='vend_product_name', column_name='name')
    handle = Field(attribute='handle', column_name='handle')
    composite_handle = Field(attribute='composite_handle', column_name='composite_handle')
    composite_sku = Field(attribute='composite_sku', column_name='composite_sku')
    composite_quantity = Field(attribute='composite_quantity', column_name='composite_quantity')
    description = Field(attribute='description', column_name='description')
    type = Field(attribute='type', column_name='type')
    variant_option_one_name = Field(column_name='variant_option_one_name')
    variant_option_one_value = Field(attribute='variant_option_one_value', column_name='variant_option_one_value')
    variant_option_two_name = Field(attribute='variant_option_two_name', column_name='variant_option_two_name')
    variant_option_two_value = Field(attribute='variant_option_two_value', column_name='variant_option_two_value')
    variant_option_three_name = Field(attribute='variant_option_three_name', column_name='variant_option_three_name')
    variant_option_three_value = Field(attribute='variant_option_three_value', column_name='variant_option_three_value')
    supply_price = Field(attribute='supply_price', column_name='supply_price')
    retail_price = Field(attribute='retail_price', column_name='retail_price')
    outlet_tax_redwood_falls = Field(attribute='outlet_tax_redwood_falls', column_name='outlet_tax_Redwood_Falls')
    account_code = Field(attribute='account_code', column_name='account_code')
    account_code_purchase = Field(attribute='account_code_purchase', column_name='account_code_purchase')
    brand = Field(attribute='brand', column_name='brand_name')
    supplier_name = Field(attribute='vendor', column_name='supplier_name')
    track_inventory = Field(attribute='track_inventory', column_name='track_inventory')

    class Meta:
        model = models.VendProduct
        export_order = ('id', 'handle', 'sku', 'composite_handle', 'composite_sku', 'composite_quantity',
                        'name', 'description', 'type', 'variant_option_one_name', 'variant_option_one_value',
                        'variant_option_two_name', 'variant_option_two_value', 'variant_option_three_name',
                        'variant_option_three_value', 'tags', 'supply_price', 'retail_price',
                        'outlet_tax_redwood_falls', 'account_code', 'account_code_purchase', 'brand',
                        'supplier_name', 'active', 'track_inventory')
        fields = export_order

    def dehydrate_id(self, v_product):
        return ''

    def dehydrate_brand(self, v_product):
        if not v_product.brand:
            return ''
        return v_product.brand.name

    def dehydrate_variant_option_one_name(self, v_product):
        if not v_product.variant_option_one_value:
            return ''
        return v_product.variant_option_one_name


class OrderResource(resources.ModelResource):
    purchased_by = Field(attribute='purchased_by', column_name='Purchased by')
    vendor = Field(attribute='vendor', column_name='Vendor')
    purchase_date = Field(attribute='purchase_date', column_name='Purchase date')
    entered_by = Field(attribute='entered_by', column_name='Entered by')
    payment_method = Field(attribute='payment_method', column_name='Payment method')
    extra_charges = Field(attribute='extra_charges', column_name='Extra charges')
    tax_amount = Field(attribute='tax_amount', column_name='Tax amount')
    receipt_total = Field(attribute='receipt_total', column_name='Receipt total')
    comments = Field(attribute='comments', column_name='Comments')

    class Meta:
        model = models.Order
        export_order = ('id', 'purchased_by', 'vendor', 'purchase_date', 'entered_by', 'payment_method',
                        'extra_charges', 'tax_amount', 'receipt_total', 'comments')
        fields = ('id', 'purchased_by', 'vendor', 'purchase_date', 'entered_by', 'payment_method',
                  'extra_charges', 'tax_amount', 'receipt_total', 'comments')


class ProductResource(resources.ModelResource):
    sku = Field(attribute='sku', column_name='sku')
    retail_quantity = Field(attribute='retail_quantity', column_name='quantity')
    vend_cost_each = Field(attribute='vend_cost_each', column_name='supply_price')

    class Meta:
        model = models.Product
        export_order = ('sku', 'retail_quantity', 'vend_cost_each')
        fields = export_order


class ExistingProductResource(resources.ModelResource):
    purchased_by = Field(attribute='purchased_by', column_name='Purchased by')
    vendor = Field(attribute='vendor', column_name='Vendor')
    type = Field(attribute='type', column_name='type')
    brand = Field(attribute='brand', column_name='brand_name')

    class Meta:
        model = models.ExistingProduct
        export_order = ('id', 'purchased_by', 'vendor', 'purchase_quantity', 'cost_each', 'we_payed_tax',
                        'taxable_product', 'brand', 'flavor', 'description', 'bought_by_pound',
                        'expiration_date', 'sku', 'type', 'needs_repacking', 'units_per_bulk',
                        'retail_price', 'comments')
        fields = ('id', 'purchased_by', 'vendor', 'purchase_quantity', 'cost_each', 'we_payed_tax',
                  'taxable_product', 'brand', 'flavor', 'description', 'bought_by_pound',
                  'expiration_date', 'sku', 'type', 'needs_repacking', 'units_per_bulk',
                  'retail_price', 'comments')

    def dehydrate_brand(self, e_product):
        if not e_product.brand:
            return ''
        return e_product.brand.name
