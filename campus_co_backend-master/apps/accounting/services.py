from apps.accounting import models


def create_products_on_order_complete(order):
    purchased_by = order.purchased_by
    vendor = order.vendor
    products = order.product_set.filter(deleted=False)
    sku_list = [p.sku for p in products if p.sku]
    models.ExistingProduct.objects.filter(sku__in=sku_list).delete()
    for product in order.product_set.filter(deleted=False):
        models.ExistingProduct.from_product(product, purchased_by, vendor).save()
        if not models.VendProduct.objects.filter(sku=product.sku).exists():
            models.VendProduct.from_product(product, vendor).save()

