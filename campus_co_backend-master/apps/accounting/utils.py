import csv

from decimal import Decimal
from io import StringIO
from pprint import pprint

from apps.accounting import resources
from apps.accounting import models
from apps.accounting import forms


class FieldCleaningException(Exception):
    pass


def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l) + 1, n):
        yield l[i:i + n]


class CSVModel(object):
    model = None
    form = None
    fields_map = None
    delimiter = ','

    def __init__(self):
        self.items = []

    def clean_field(self, field, val):
        func = getattr(self, f'clean_{field}', None)
        if not func:
            return val
        return func(val)

    def process_item(self, data):
        to_save = {}
        for key, val in data.items():
            field = self.fields_map.get(key)
            if not field:
                continue
            try:
                to_save.update({field: self.clean_field(field, val)})
            except Exception:
                print(f'Exception during cleaning {field} with value {val}')

        form = self.form(to_save)
        is_valid = form.is_valid()
        if not is_valid:
            raise FieldCleaningException(form.errors)
        self.items.append(self.model(**to_save))

    def save(self):
        self.model.objects.bulk_create(self.items)
        self.items = []


class VendProductCSVModel(CSVModel):
    model = models.VendProduct
    form = forms.VendProductForm
    resource = resources.VendProductResource

    def __init__(self):
        self.fields_map = {v.column_name: v.attribute or k for k, v in self.resource().fields.items()}
        super().__init__()

    def clean_retail_price(self, val: str):
        return Decimal(val.replace('$', '').replace(',', ''))

    def clean_id(self, val: str):
        return None

    def clean_vendor(self, val: str):
        return models.Vendor.objects.filter(name=val).first()

    def clean_brand(self, val: str):
        return models.Brand.objects.filter(name=val).first()

    def clean_type(self, val: str):
        return models.ProductType.objects.filter(name=val).first()


class EmployeeCSVModel(CSVModel):
    model = models.Employee
    form = forms.EmployeeForm
    fields_map = {'first_name': 'first_name', 'last_name': 'last_name'}


class ProductTypeCSVModel(CSVModel):
    model = models.ProductType
    form = forms.ProductTypeForm
    delimiter = ','
    fields_map = {'Type': 'name', 'Brand': 'brand'}

    def clean_brand(self, val):
        return models.Brand.objects.filter(name=val).first()


class VendorCSVModel(CSVModel):
    model = models.Vendor
    form = forms.ProductTypeForm
    delimiter = ';'
    fields_map = {'name': 'name'}


class ImportCSVHelper(object):
    def __init__(self, model, data):
        self.model = model()
        self.errors = []
        file = data.read().decode('utf-8')
        self.io = list(csv.DictReader(StringIO(file), delimiter=self.model.delimiter))

    def save(self):
        for chunk in chunks(self.io, 20):
            for row in chunk:
                try:
                    self.model.process_item(row)
                except FieldCleaningException:
                    self.errors.append(row)
            self.model.save()
        pprint(f'Errors {self.errors}')

