from django.urls import path, include


urlpatterns = [
    path('users/', include('apps.users.api.urls')),
    path('accounting/', include('apps.accounting.api.urls')),
]
