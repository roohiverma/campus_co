from rest_framework.authtoken.models import Token
from rest_framework.authtoken.serializers import AuthTokenSerializer
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView

from apps.users.api.serializers import UserSerializer


class ObtainAuthToken(APIView):
    permission_classes = (AllowAny,)
    serializer_class = AuthTokenSerializer

    @staticmethod
    def refresh_token_if_exists(request, user):
        """Refresh token when user login."""
        Token.objects.filter(user=user).delete()
        return Token.objects.create(user=user)

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token = self.refresh_token_if_exists(request, user)
        return Response({
            'token': token.key,
            'user': UserSerializer(user, context={'request': request}).data
        })


