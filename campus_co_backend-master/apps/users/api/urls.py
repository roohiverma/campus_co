from django.urls import path

from apps.users.api import endpoints


urlpatterns = [
    path('auth/token/', endpoints.ObtainAuthToken.as_view(), name='retrieve-token'),
]
