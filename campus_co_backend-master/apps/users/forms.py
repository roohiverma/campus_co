from django import forms
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from django.db import transaction

from apps.users.models import User
from apps.users.notifications import notify_register_complete


class UserAdminCreationForm(forms.ModelForm):
    """A form for creating new users. Includes all the required
    fields, plus a repeated password."""
    password1 = forms.CharField(label='Password', required=False, widget=forms.PasswordInput)
    password2 = forms.CharField(label='Password confirmation', required=False, widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ('email',)

    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 != password2:
            raise forms.ValidationError("Passwords don't match")
        return password2

    def save(self, commit=True, notify=False):
        # Save the provided password in hashed format
        user = super(UserAdminCreationForm, self).save(commit=False)
        password = self.cleaned_data["password1"]
        if not password:
            password = User.objects.make_random_password()
            notify = True
        user.set_password(password)
        if commit:
            user.save()
        if notify:
            transaction.on_commit(lambda: notify_register_complete(user, password))
        return user


class UserAdminChangeForm(forms.ModelForm):
    """A form for updating users. Includes all the fields on
    the user, but replaces the password field with admin's
    password hash display field.
    """
    password = ReadOnlyPasswordHashField()

    class Meta:
        model = User
        fields = ('email', 'password', 'active', 'admin', 'staff', 'notify_on_order_imported')

    def clean_password(self):
        # Regardless of what the user provides, return the initial value.
        # This is done here, rather than on the field, because the
        # field does not have access to the initial value
        return self.initial["password"]
