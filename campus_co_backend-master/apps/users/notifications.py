from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.mail import send_mail

User = get_user_model()


def notify_register_complete(user, password):
    send_mail(f"Registration at {settings.ADMIN_SITE_HEADER}",
              f"""
              You are registered at {settings.ADMIN_SITE_HEADER} system.
              Email: {user.email}
              Password: {password}
              Link: {settings.SITE_SHAME}://{settings.SITE_HOST}/admin/
              
              Also you need to ask your system administrator to provide you access. 
              Please change your password after login!
              """,
              f"{settings.ADMIN_SITE_HEADER} <ben@ntstiresupply.com>", [user.email, ])
