INSTALLED_APPS = [
    'jet',

    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    # libs
    'django_extensions',
    'corsheaders',
    'rest_framework',
    'rest_framework.authtoken',
    'import_export',

    # project apps
    'apps.users',
    'apps.api',
    'apps.accounting',
]
