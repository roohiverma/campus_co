from main.components.apps import INSTALLED_APPS
from main.components.common import MIDDLEWARE

DEBUG = True

ALLOWED_HOSTS = ['*']

INSTALLED_APPS += (
    'debug_toolbar',
)

CORS_ORIGIN_ALLOW_ALL = True

MIDDLEWARE += (
    'debug_toolbar.middleware.DebugToolbarMiddleware',
)


def show_toolbar_callback(request):
    """Only show the debug toolbar to users with the superuser flag."""
    return True


DEBUG_TOOLBAR_CONFIG = {
    'SHOW_TOOLBAR_CALLBACK':
        'main.environments.development.show_toolbar_callback',
}
