import os

from main.components import env as config, BASE_DIR

DEBUG = False

ALLOWED_HOSTS = [
    config('DOMAIN_NAME'),
]

_PASS = 'django.contrib.auth.password_validation'
AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': '{0}.UserAttributeSimilarityValidator'.format(_PASS),
    },
    {
        'NAME': '{0}.MinimumLengthValidator'.format(_PASS),
    },
    {
        'NAME': '{0}.CommonPasswordValidator'.format(_PASS),
    },
    {
        'NAME': '{0}.NumericPasswordValidator'.format(_PASS),
    },
]

STATIC_ROOT = os.path.join(BASE_DIR, 'static')

# SECURE_HSTS_SECONDS = 518400
# SECURE_HSTS_INCLUDE_SUBDOMAINS = True
# SECURE_HSTS_PRELOAD = True
# SECURE_CONTENT_TYPE_NOSNIFF = True
# SECURE_BROWSER_XSS_FILTER = True
#
# SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')
# SECURE_SSL_REDIRECT = True
#
# SESSION_COOKIE_SECURE = True
# SESSION_COOKIE_HTTPONLY = True
#
# CSRF_COOKIE_SECURE = True
# CSRF_COOKIE_HTTPONLY = True
#
# X_FRAME_OPTIONS = 'DENY'
