from split_settings.tools import optional, include
from main.components import env as config

ENV = config('DJANGO_ENV')

include(
    'components/common.py',
    'components/apps.py',
    'components/database.py',

    'environments/{env}.py'.format(env=ENV),

    optional('environments/local.py')
)
