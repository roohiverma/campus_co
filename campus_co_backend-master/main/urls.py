from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include
from rest_framework.documentation import include_docs_urls

admin.site.site_header = settings.ADMIN_SITE_HEADER

urlpatterns = [

    path('admin/', admin.site.urls),

    path('jet/', include('jet.urls', 'jet')),

    path('docs/', include_docs_urls(title=settings.ADMIN_SITE_HEADER)),
    path('api/', include('apps.api.urls'))
]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns.append(
        path('__debug__/', include(debug_toolbar.urls))
    )
    urlpatterns += static(settings.STATIC_URL,
                          document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)
